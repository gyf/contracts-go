package tools

import (
	"encoding/json"
	"fmt"
	"reflect"
	"unsafe"
)

import (
	"github.com/spf13/cast"
)

type Result struct {
	Code int32       `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func ToResult(data interface{}) []byte {
	dataRet := &Result{
		Code: 200,
		Msg:  "",
		Data: data,
	}
	res, err := json.Marshal(dataRet)
	if err != nil {
		v := reflect.ValueOf(data)
		k := v.Kind()
		switch k {
		case reflect.Bool, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint,
			reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Float32, reflect.Float64:
			res = []byte(cast.ToString(v.Interface()))
		case reflect.Array, reflect.Interface, reflect.Map, reflect.Slice, reflect.String, reflect.Struct:
			res = StructToBytes(data, int(unsafe.Sizeof(data)))
		default:
			res = []byte(fmt.Sprintf("unknow value is %s\n", k.String()))
		}
	}
	return res
}

// StructToBytes struct转为[]byte
func StructToBytes(iter interface{}, len int) []byte {
	var x reflect.SliceHeader
	x.Len = len
	x.Cap = len
	x.Data = reflect.ValueOf(iter).Pointer()
	return *(*[]byte)(unsafe.Pointer(&x))
}

// BytesToStruct []byte转为struct
func BytesToStruct(buf []byte) unsafe.Pointer {
	return unsafe.Pointer(
		(*reflect.SliceHeader)(unsafe.Pointer(&buf)).Data,
	)
}

// Bytes2str
func Bytes2str(b []byte) string {
	return *(*string)(unsafe.Pointer(&b))
}

// Str2bytes
func Str2bytes(s string) []byte {
	// 获取s的起始地址开始后的两个 uint_ptr 指针
	x := (*[2]uintptr)(unsafe.Pointer(&s))
	// 构造三个指针数组
	h := [3]uintptr{x[0], x[1], x[1]}
	return *(*[]byte)(unsafe.Pointer(&h))
}
