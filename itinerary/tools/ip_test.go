package tools

import (
	"fmt"
	"strings"
	"testing"
)

func TestGetLocationByIp(t *testing.T) {
	serverUrl := []string{
		"https://myexternalip.com/raw",
		"https://www.eskysky.com/web/getip/",
		"https://4.ipw.cn/",
		"https://ip.gs/",
		"https://ident.me/",
		"https://ifconfig.me",
		"https://icanhazip.com",
		"https://ipecho.net/plain",
		"https://tnx.nl/ip",
	}

	externalIp := ""
	for idx, server := range serverUrl {
		externalIp = getExternal(server)
		fmt.Printf("%d: %s, server=%s\n", idx, strings.TrimSpace(externalIp), server)
		if externalIp != "" {
			GetLocationByIp(externalIp)
			break
		}
	}
}

func TestGetIpInfo(t *testing.T) {
	GetIpInfo()
}
